#!/bin/bash

path=$(readlink -f $1) #Chemin absolu du fichier en paramètre
pathArch=/tmp/$(date +%Y_%m_%d_%H_%M)_$(basename $1).tar.gz #Chemin de l'archive créer
tar -cvf $pathArch $1 >/dev/null 2>&1   #Création de l'archive
echo "Création de l'archive" $pathArch
