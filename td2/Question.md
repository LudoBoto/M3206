## Question sur le TD de find

__Que fait la commande suivante : find /tmp -name *.txt -ok rm {} \; ?__
__Que fait l'option -ok ?__
	Cette commande trouve tous les fichiers au format .txt qui se situent dans le dossier /tmp. L'otion -ok va demander la permission de les supprimer, seulement si le fichier existe. Si on met Y sa supprime et si on laisse vide sa ne supprime pas.

__Que fait la commande cp ?__
	Cette commande nous permet de copier un fichier ou un dossier d'un emplacement source vers un emplacement de destination. La commande permettant de copier tous les répertoires d'un fichier vers un autredossier est : `find <Dossier> -ok cp <Destination> {} \;`

Trouvez la commande qui permet de lister les premières ligne des fichiers *.txt d'un répertoire.
	La commande à utiliser est :

```bash
find <Dossier> -name "*.txt" -exec head -n <Nb ligne> {} \;
```
Il ne faut pas oublier les guillemets !
