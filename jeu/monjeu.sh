#!/bin/bash

if [ "$0" == "./monjeu.sh" ]; then
    echo "Le script est nommé et lancé correctement"
else
    echo "Modifier le nom de ce script en monjeu.sh et exécuter le avec ./monjeu.sh"
    exit 1
fi

if [ -f "test.txt" ]; then
	echo "le fichier existe"
else
	echo "créer un fichier test.txt dans le répertoire actuel"
	exit 1
fi

perm=$(ls -l test.txt | awk '{print $1}')
if [ $perm == '-rw-r-----' ]; then
	echo "Les permissions son correcte"
else
	echo "Veuillez mettre la permission 640"
fi

cat compteur | awk 'BEGIN {c=1}{if c=1}{if($1 != c){print "erreur"}; c=c+1 ; } '> err
 
