### Voici les commandes que l'utilisateur devra effectuer pour continuer à jouer.

La première commande que l'utilisateur devra taper est :
```bash
mv jeu.sh monjeu.sh
```

La deuxième commande que l'utilisateur devra taper est :
```bash
vi test.txt
```
Sans oublier de le remplir bien sûr.

La troixème commande que l'utilisateur devra taper est :
```bash
chmod 640 test.txt
```

La quatrième commande que l'utilisateur devra taper est :
```bash
for i in $(seq 1 1 1000); do echo $i; done >> compteur
```
