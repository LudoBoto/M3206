#!/bin/bash

if (( $EUID == 0 )); then
	echo "[...] update system [...]"	
	apt-get update -q  >/dev/null	#lance l'update

	echo "[...]Upgrade systeme[...]"
	apt-get upgrade -q -y >/dev/null	#lance l'upgrade et met Y pour la question 'Do youwant to continue (Y/N):
else
	echo "[/!\]Vous devez être root[/!\]"
fi
