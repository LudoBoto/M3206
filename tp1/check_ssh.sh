#!/bin/bash

dpkg -l | grep ssh >/dev/null 2>&1
if [ $? == 0 ];then
	/etc/init.d/ssh status >/dev/null 2>&1
	if [ $? == 0 ];then
		echo "[...]ssh: ssh fonctionne[...]"
	else
		echo "[...]ssh: installé[...]
[/!\]ssh: service non lancé[/!\]
[...]ssh: lancement du service[...] tapé /etc/init.d/ssh start"
	fi
else
	echo "[/!\]ssh: pas installé[/!\]"
fi
