#!/bin/bash
#vérifie si git est installé
dpkg -l | grep git >/dev/null 2>&1
if [ $? == 0 ]; then
	echo "[...]git: installé[...]"
else 
	echo "[/!\]git: pas installé[/!\] éxécuter apt-get install git"
fi

#vérifie si tmux est installé
dpkg -l | grep tmux >/dev/null 2>&1
if [ $? == 0 ]; then
	echo "[...]tmux: installé[...]"
else
	echo "[/!\]tmux: pas installé[/!\] éxécuté apt-get install tmux"
fi

#vérifie si vim est installé
dpkg -l | grep vim >/dev/nul 2>&1
if [ $? == 0 ]; then
	echo "[...]vim : installé[...]"
else
	echo "[/:\]vim : pas installé[/!\] éxécuté apt-get install vim"
fi

#vérifie si htop est installé
dpkg -l | grep htop >/dev/null 2>&1
if [ $? == 0 ]; then
	echo "[...]htop: installé[...]"
else
	echo "[/!\]htop: pas installé[/!\] éxécuté apt-get install htop"
fi
