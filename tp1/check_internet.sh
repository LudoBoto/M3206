#!/bin/bash

echo "[...]Checking connection[...]"
#Test de connectivité avec google.com
ping -q -c 2 www.google.com >/dev/null 2>&1

#Condition
#  test la valeur du ping, si il retourne 0 c'est bon

if [ $? -eq 0 ]; then

#Affiche à l'écran si nous sommes connecté ou non..

	echo "[...]Internet Acces OK[...]"
else
	echo "[/!\]Not Connected to Internet[/!\]
[/!\]Please check configuration[/!\]"
fi
